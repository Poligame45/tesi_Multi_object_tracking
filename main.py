import cv2
import norfair
from norfair import Tracker
from helper import yolo_detections_to_norfair_detections
import time
import matplotlib.pyplot as plt
import numpy as np
from yolo import YOLO as YOLOv8
import ffmpeg

DISTANCE_THRESHOLD_BBOX: float = 0.7
DISTANCE_THRESHOLD_CENTROID: int = 30
MAX_DISTANCE: int = 10000
V8 = False
OUTPUT_FOLDER = "output"


# Funzione per rilevare oggetti con YOLOv5

model = YOLOv8("yolov5nu.pt", "cpu")
model.names = 0

tracker = Tracker("iou",0.7)

# Create a VideoCapture object and read from input file
# If the input is the camera, pass 0 instead of the video file name
#cap = cv2.VideoCapture('http://83.56.31.69/mjpg/video.mjpg')
#cap = cv2.VideoCapture('http://185.112.166.158/mjpg/video.mjpg')
#cap = cv2.VideoCapture('http://166.130.18.45:1024/mjpg/video.mjpg?camera=1&timestamp=1715810692293')
#cap = cv2.VideoCapture(0)
#cap = cv2.VideoCapture("./video1.mp4")
cap = cv2.VideoCapture("./video.mp4")
results = []
index = []
start_time = time.time()
# Check if camera opened successfully
if (cap.isOpened() == False):
    print("Error opening video stream or file")

# Read until video is completed
while (cap.isOpened()):
    # Capture frame-by-frame
    ret, frame = cap.read()
    if ret == True:

        #rileva oggetti nel frame con yolo
        yolo_detections = model( #per ogni frame nel video eseguo la funzione "Call", in modo da eseguire il tracking e riporta i risultati
            frame,
            0.25,
            0.45,
            480,
            [0]
        )
        detections = yolo_detections_to_norfair_detections( #Trasforma il rilevamento di Yolo in rilevamento Norfair, cambia le coordinate secondo la lib di Norfair
            yolo_detections, "bbox", True
        )

        # Aggiorna il tracker con le nuove rilevazioni
        tracked_objects = tracker.update(detections)

        # Disegna i box dei tracker sul frame
        norfair.draw_boxes(frame, detections)
        norfair.draw_tracked_boxes(frame, tracked_objects)

        # Visualizza il frame
        cv2.imshow('http', frame)

        #Costruisco il grafico
        current_time=time.time()-start_time
        if int(current_time) not in index and current_time>1:
            results.append(len(detections)) #Aggiungi all'array dei risultati il numero delle persone rilevate.
            print(f"current time {current_time} ")
            index.append(int(current_time))
        # Press Q on keyboard to  exit
        if cv2.waitKey(25) & 0xFF == ord('q'):
            break

    # Break the loop
    else:
        break
end_time = time.time()
elapsed_time = end_time - start_time

print(results)
print(index)

x = index
y = results
plt.plot(x, y)

# Aggiungi etichette agli assi e un titolo
plt.xlabel('Tempo di rilevazione')
plt.ylabel('Persone rilevate')
plt.title('Risultati del tracking')

plt.savefig('grafico.png')  # Salva il grafico come un file di immagine
# When everything done, release the video capture object
cap.release()

# Closes all the frames
cv2.destroyAllWindows()