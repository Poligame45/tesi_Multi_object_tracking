from typing import List
import numpy as np
from norfair import Detection
import torch

def center(points):
    return [np.mean(np.array(points), axis=0)]


def yolo_detections_to_norfair_detections(
        yolo_detections: torch.tensor, track_points: str = "centroid",  # bbox or centroid
        V8: bool = False
) -> List[Detection]:
    """convert detections_as_xywh to norfair detections"""
    norfair_detections: List[Detection] = []

    if track_points == "centroid":
        if V8:
            detections_as_xywh = yolo_detections.xywh
        else:
            detections_as_xywh = yolo_detections.xywh[0]

        for detection_as_xywh in detections_as_xywh:
            centroid = np.array(
                [detection_as_xywh[0].item(), detection_as_xywh[1].item()]
            )  #trasforma le coordinate in numeri

            if not V8:
                scores = np.array([detection_as_xywh[4].item()])  # prendi i punteggi e li salvi
                norfair_detections.append(
                    Detection(
                        points=centroid,
                        scores=scores,
                        label=int(detection_as_xywh[-1].item()),
                    )
                )
            else:
                norfair_detections.append(
                    Detection(
                        points=centroid
                    )
                )
    elif track_points == "bbox":
        if V8:
            detections_as_xyxy = yolo_detections.xyxy
        else:
            detections_as_xyxy = yolo_detections.xyxy[0]

        for detection_as_xyxy in detections_as_xyxy:
            bbox = np.array(
                [
                    [detection_as_xyxy[0].item(), detection_as_xyxy[1].item()],
                    [detection_as_xyxy[2].item(), detection_as_xyxy[3].item()],
                ]
            )
            if not V8:
                scores = np.array(
                    [detection_as_xyxy[4].item(), detection_as_xyxy[4].item()]
                )

                norfair_detections.append(
                    Detection(
                        points=bbox, scores=scores, label=int(detection_as_xyxy[-1].item())
                    )  #aggiungo alla lista di ritorno, se uso yolo5 aggiungo anche il punteggio e/o label
                )
            else:
                norfair_detections.append(
                    Detection(
                        points=bbox
                    )
                )  # altrimenti se uso yolo8 restituisco solo le coordinate

    return norfair_detections
